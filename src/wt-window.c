/* wt-window.c
 *
 * Copyright 2023 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "wt-window.h"
#include "wt-sidebar-row.h"

struct _WatchtowerWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  AdwHeaderBar        *sidebar_header_bar;
  AdwHeaderBar        *content_header_bar;
};

G_DEFINE_FINAL_TYPE (WatchtowerWindow, watchtower_window, ADW_TYPE_APPLICATION_WINDOW)

static void
watchtower_window_class_init (WatchtowerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkCssProvider *provider = gtk_css_provider_new ();

  g_type_ensure (WATCHTOWER_TYPE_SIDEBAR_ROW);

  gtk_css_provider_load_from_resource (provider, "/org/gnome/Watchtower/watchtower.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (), GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Watchtower/watchtower-window.ui");
  gtk_widget_class_bind_template_child (widget_class, WatchtowerWindow, sidebar_header_bar);
  gtk_widget_class_bind_template_child (widget_class, WatchtowerWindow, content_header_bar);
}

static void
watchtower_window_init (WatchtowerWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
