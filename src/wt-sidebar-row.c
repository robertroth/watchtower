/* wt-sidebar-row.c
 *
 * Copyright 2023 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "wt-sidebar-row.h"

typedef enum
{
  PROP_LABEL = 1,
  LAST_PROP
} WatchtowerSidebarRowProperty;

static GParamSpec *props[LAST_PROP] = { NULL, };

struct _WatchtowerSidebarRow
{
  GtkListBoxRow  parent_instance;
};

typedef struct
{
  /* Template widgets */
  GtkLabel        *label;
} WatchtowerSidebarRowPrivate;

G_DEFINE_FINAL_TYPE_WITH_PRIVATE (WatchtowerSidebarRow, watchtower_sidebar_row, GTK_TYPE_LIST_BOX_ROW)

const char *
watchtower_sidebar_row_get_label (WatchtowerSidebarRow *self)
{
  WatchtowerSidebarRowPrivate *priv;

  g_return_val_if_fail (WATCHTOWER_IS_SIDEBAR_ROW (self), NULL);

  priv = watchtower_sidebar_row_get_instance_private (self);

  return gtk_label_get_text (priv->label);
}

void
watchtower_sidebar_row_set_label (WatchtowerSidebarRow *self,
                                  const char   *label)
{
  WatchtowerSidebarRowPrivate *priv;

  g_return_if_fail (WATCHTOWER_IS_SIDEBAR_ROW (self));

  priv = watchtower_sidebar_row_get_instance_private (self);

  if (g_strcmp0 (gtk_label_get_text (priv->label), label) == 0)
    return;

  gtk_label_set_label (priv->label, label);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_LABEL]);
}

static void
watchtower_sidebar_row_set_property (GObject      *object,
                                     guint         property_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  WatchtowerSidebarRow *self = WATCHTOWER_SIDEBAR_ROW (object);

  switch ((WatchtowerSidebarRowProperty) property_id)
    {
    case PROP_LABEL:
      watchtower_sidebar_row_set_label (self, g_value_dup_string (value));
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
watchtower_sidebar_row_get_property (GObject    *object,
                                     guint       property_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  WatchtowerSidebarRow *self = WATCHTOWER_SIDEBAR_ROW (object);

  switch ((WatchtowerSidebarRowProperty) property_id)
    {
    case PROP_LABEL:
      g_value_set_string (value, watchtower_sidebar_row_get_label (self));
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
watchtower_sidebar_row_class_init (WatchtowerSidebarRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  
  object_class->set_property = watchtower_sidebar_row_set_property;
  object_class->get_property = watchtower_sidebar_row_get_property;
  
  props[PROP_LABEL] =
    g_param_spec_string ("label",
                         "Label",
                         "Label to show",
                         NULL  /* default value */,
                         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class,
                                      LAST_PROP,
                                      props);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Watchtower/watchtower-sidebar-row.ui");
  gtk_widget_class_bind_template_child_private (widget_class, WatchtowerSidebarRow, label);
}

static void
watchtower_sidebar_row_init (WatchtowerSidebarRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
