/* wt-sidebar-row.h
 *
 * Copyright 2023 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include <adwaita.h>
#include <glib/gi18n.h>

G_BEGIN_DECLS

#define WATCHTOWER_TYPE_SIDEBAR_ROW (watchtower_sidebar_row_get_type())

G_DECLARE_FINAL_TYPE (WatchtowerSidebarRow, watchtower_sidebar_row, WATCHTOWER, SIDEBAR_ROW, GtkListBoxRow)

const char *
watchtower_sidebar_row_get_label (WatchtowerSidebarRow *self);

void
watchtower_sidebar_row_set_label (WatchtowerSidebarRow *self,
                                  const char   *label);

G_END_DECLS
